docker
======

This repo contains various build scripts for docker images.

latex
-----

Script (including `Dockerfile`) to build Docker image used for
processing LaTeX source files in various CI/CD pipelines

refc
----

Script (including `Dockerfile`) to build Docker image used as
a reference version of the compiler for various courses on
Operating Systems.

amc
---

auto-multiple-choice

moodlenq
--------

Use moodlenq (see https://gitlab.com/pdagog/moodlenq) without
installing Python prerequisites.
